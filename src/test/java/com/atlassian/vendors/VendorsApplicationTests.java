package com.atlassian.vendors;

import com.atlassian.vendors.controllers.AccountController;
import com.atlassian.vendors.controllers.ContactController;
import com.atlassian.vendors.objects.Account;
import com.atlassian.vendors.objects.Contact;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {VendorsApplication.class, TestProfile.class})
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class VendorsApplicationTests {

    @Autowired
    private AccountController accountController;

    @Autowired
    private ContactController contactController;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void contextLoads() {
        assertThat(accountController).isNotNull();
        assertThat(contactController).isNotNull();
    }

    @Test
    public void apiTests() throws Exception {
        // verify DB is empty and request works
        this.mockMvc
                .perform(get("/api/v1/account"))
                .andExpect(status().isOk())
                .andExpect(content().string("[]"));

        // verify error handler on no message body
        this.mockMvc
                .perform(post("/api/v1/account"))
                .andExpect(status().isBadRequest());

        // verify error on empty Account object
        this.mockMvc
                .perform(
                        post("/api/v1/account")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{}"))
                .andExpect(status().isBadRequest())
                .andExpect(content().string(containsString("Required Account fields are not present")));

        // verify success in adding account
        this.mockMvc
                .perform(
                        post("/api/v1/account")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content("{\n" +
                                        "\t\"name\": \"Test Account\",\n" +
                                        "\t\"address1\": \"Some Address\",\n" +
                                        "\t\"city\": \"Kansas City\",\n" +
                                        "\t\"state\": \"Missouri\",\n" +
                                        "\t\"postalCode\": \"64110\",\n" +
                                        "\t\"country\": \"USA\"\n" +
                                        "}"))
                .andExpect(status().isCreated())
                .andExpect(content().string(containsString("\"id\":1")));

        // verify duplicate accounts can't be added
        this.mockMvc
                .perform(
                        post("/api/v1/account")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content("{\n" +
                                        "\t\"name\": \"Test Account\",\n" +
                                        "\t\"address1\": \"Some Address\",\n" +
                                        "\t\"city\": \"Kansas City\",\n" +
                                        "\t\"state\": \"Missouri\",\n" +
                                        "\t\"postalCode\": \"64110\",\n" +
                                        "\t\"country\": \"USA\"\n" +
                                        "}"))
                .andExpect(status().isConflict())
                .andExpect(content().string(containsString("Account already exists with that name")));

        // update account in DB
        this.mockMvc
                .perform(
                        put("/api/v1/account")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content("{\n" +
                                        "\t\"id\": 1,\n" +
                                        "\t\"name\": \"Updated Account\",\n" +
                                        "\t\"address1\": \"Some Address\",\n" +
                                        "\t\"city\": \"Kansas City\",\n" +
                                        "\t\"state\": \"Missouri\",\n" +
                                        "\t\"postalCode\": \"64110\",\n" +
                                        "\t\"country\": \"USA\"\n" +
                                        "}"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("\"name\":\"Updated Account\"")));

        // verify Contact fields are checked properly before insertion
        this.mockMvc
                .perform(
                        post("/api/v1/contact")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content("{}"))
                .andExpect(status().isBadRequest())
                .andExpect(content().string(containsString("Required Contact fields are not present")));

        // add valid contact with no account connection
        this.mockMvc
                .perform(
                        post("/api/v1/contact")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content("{\n" +
                                        "\t\"name\": \"Josh Ellis\",\n" +
                                        "\t\"email\": \"josh@ellis.sh\",\n" +
                                        "\t\"address1\": \"6133 Some Rd\",\n" +
                                        "\t\"city\": \"Kansas City\",\n" +
                                        "\t\"state\": \"Missouri\",\n" +
                                        "\t\"postalCode\": \"64110\",\n" +
                                        "\t\"country\": \"USA\"\n" +
                                        "}"))
                .andExpect(status().isCreated())
                .andExpect(content().string(containsString("\"id\":2")));

        // verify duplicate emails can't be used
        this.mockMvc
                .perform(
                        post("/api/v1/contact")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content("{\n" +
                                        "\t\"name\": \"Josh Ellis\",\n" +
                                        "\t\"email\": \"josh@ellis.sh\",\n" +
                                        "\t\"address1\": \"6133 Some Rd\",\n" +
                                        "\t\"city\": \"Kansas City\",\n" +
                                        "\t\"state\": \"Missouri\",\n" +
                                        "\t\"postalCode\": \"64110\",\n" +
                                        "\t\"country\": \"USA\"\n" +
                                        "}"))
                .andExpect(status().isConflict())
                .andExpect(content().string(containsString("Contact already exists with that email address")));

        // add valid contact associated with account
        this.mockMvc
                .perform(
                        post("/api/v1/contact")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content("{\n" +
                                        "\t\"name\": \"Josh Ellis\",\n" +
                                        "\t\"email\": \"neuroclast@gmail.com\",\n" +
                                        "\t\"address1\": \"6133 Some Rd\",\n" +
                                        "\t\"city\": \"Kansas City\",\n" +
                                        "\t\"state\": \"Missouri\",\n" +
                                        "\t\"postalCode\": \"64110\",\n" +
                                        "\t\"country\": \"USA\",\n" +
                                        "\t\"account\": {\n" +
                                        "\t\t\"id\": 1\n" +
                                        "\t}\n" +
                                        "}"))
                .andExpect(status().isCreated())
                .andExpect(content().string(containsString("\"id\":3")));

        // verify error trying to find non-existent contact
        this.mockMvc
                .perform(get("/api/v1/contact/4"))
                .andExpect(status().isNotFound());

        // verify valid contact is returned
        this.mockMvc
                .perform(get("/api/v1/contact/3"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("id\":3,\"name\":\"Josh Ellis")));

        // update account in DB
        this.mockMvc
                .perform(
                        put("/api/v1/contact")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content("{\n" +
                                        "\t\"id\": 3,\n" +
                                        "\t\"name\": \"Josh Hello\",\n" +
                                        "\t\"email\": \"neuroclast@gmail.com\",\n" +
                                        "\t\"address1\": \"6133 Some Rd\",\n" +
                                        "\t\"city\": \"Kansas City\",\n" +
                                        "\t\"state\": \"Missouri\",\n" +
                                        "\t\"postalCode\": \"64110\",\n" +
                                        "\t\"country\": \"USA\",\n" +
                                        "\t\"account\": {\n" +
                                        "\t\t\"id\": 1\n" +
                                        "\t}\n" +
                                        "}"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("name\":\"Josh Hello")));

        // verify contacts are listed properly
        this.mockMvc
                .perform(get("/api/v1/contact"))
                .andExpect(status().isOk())
                .andExpect(content().string("[{\"id\":2,\"name\":\"Josh Ellis\",\"email\":\"josh@ellis.sh\",\"address1\":\"6133 Some Rd\",\"address2\":null,\"city\":\"Kansas City\",\"state\":\"Missouri\",\"postalCode\":\"64110\",\"country\":\"USA\",\"account\":null},{\"id\":3,\"name\":\"Josh Hello\",\"email\":\"neuroclast@gmail.com\",\"address1\":\"6133 Some Rd\",\"address2\":null,\"city\":\"Kansas City\",\"state\":\"Missouri\",\"postalCode\":\"64110\",\"country\":\"USA\",\"account\":{\"id\":1}}]"));

        // list accounts in DB with their contacts
        this.mockMvc
                .perform(get("/api/v1/account?withContacts=true"))
                .andExpect(status().isOk())
                .andExpect(content().string("[{\"id\":1,\"name\":\"Updated Account\",\"address1\":\"Some Address\",\"address2\":null,\"city\":\"Kansas City\",\"state\":\"Missouri\",\"postalCode\":\"64110\",\"country\":\"USA\",\"contacts\":[{\"id\":3,\"name\":\"Josh Hello\",\"email\":\"neuroclast@gmail.com\",\"address1\":\"6133 Some Rd\",\"address2\":null,\"city\":\"Kansas City\",\"state\":\"Missouri\",\"postalCode\":\"64110\",\"country\":\"USA\"}]}]"));
    }


    @Test
    public void contactTest() {
        
        // tests for isValid()

        Contact c = new Contact();
        //c.setName("Josh Ellis");
        c.setEmail("josh@ellis.sh");
        c.setAddress1("6133 Some Rd");
        c.setCity("Kansas City");
        c.setState("Missouri");
        c.setPostalCode("64110");
        c.setCountry("USA");
        assertThat(c.isValid()).isFalse();

        c = new Contact();
        c.setName("Josh Ellis");
        //c.setEmail("josh@ellis.sh");
        c.setAddress1("6133 Some Rd");
        c.setCity("Kansas City");
        c.setState("Missouri");
        c.setPostalCode("64110");
        c.setCountry("USA");
        assertThat(c.isValid()).isFalse();

        c = new Contact();
        c.setName("Josh Ellis");
        c.setEmail("josh@ellis.sh");
        //c.setAddress1("6133 Some Rd");
        c.setCity("Kansas City");
        c.setState("Missouri");
        c.setPostalCode("64110");
        c.setCountry("USA");
        assertThat(c.isValid()).isFalse();

        c = new Contact();
        c.setName("Josh Ellis");
        c.setEmail("josh@ellis.sh");
        c.setAddress1("6133 Some Rd");
        //c.setCity("Kansas City");
        c.setState("Missouri");
        c.setPostalCode("64110");
        c.setCountry("USA");
        assertThat(c.isValid()).isFalse();

        c = new Contact();
        c.setName("Josh Ellis");
        c.setEmail("josh@ellis.sh");
        c.setAddress1("6133 Some Rd");
        c.setCity("Kansas City");
        //c.setState("Missouri");
        c.setPostalCode("64110");
        c.setCountry("USA");
        assertThat(c.isValid()).isFalse();

        c = new Contact();
        c.setName("Josh Ellis");
        c.setEmail("josh@ellis.sh");
        c.setAddress1("6133 Some Rd");
        c.setCity("Kansas City");
        c.setState("Missouri");
        //c.setPostalCode("64110");
        c.setCountry("USA");
        assertThat(c.isValid()).isFalse();

        c = new Contact();
        c.setName("Josh Ellis");
        c.setEmail("josh@ellis.sh");
        c.setAddress1("6133 Some Rd");
        c.setCity("Kansas City");
        c.setState("Missouri");
        c.setPostalCode("64110");
        //c.setCountry("USA");
        assertThat(c.isValid()).isFalse();

        c = new Contact();
        c.setName("Josh Ellis");
        c.setEmail("josh@ellis.sh");
        c.setAddress1("6133 Some Rd");
        c.setCity("Kansas City");
        c.setState("Missouri");
        c.setPostalCode("64110");
        c.setCountry("USA");
        assertThat(c.isValid()).isTrue();
    }


    @Test
    public void accountTest() {

        // tests for isValid()

        Account a = new Account();
        //a.setName("Test Account");
        a.setAddress1("6133 Some Rd");
        a.setCity("Kansas City");
        a.setState("Missouri");
        a.setPostalCode("64110");
        a.setCountry("USA");
        assertThat(a.isValid()).isFalse();

        a = new Account();
        a.setName("Test Account");
        //c.setAddress1("6133 Some Rd");
        a.setCity("Kansas City");
        a.setState("Missouri");
        a.setPostalCode("64110");
        a.setCountry("USA");
        assertThat(a.isValid()).isFalse();

        a = new Account();
        a.setName("Test Account");
        a.setAddress1("6133 Some Rd");
        //c.setCity("Kansas City");
        a.setState("Missouri");
        a.setPostalCode("64110");
        a.setCountry("USA");
        assertThat(a.isValid()).isFalse();

        a = new Account();
        a.setName("Test Account");
        a.setAddress1("6133 Some Rd");
        a.setCity("Kansas City");
        //c.setState("Missouri");
        a.setPostalCode("64110");
        a.setCountry("USA");
        assertThat(a.isValid()).isFalse();

        a = new Account();
        a.setName("Test Account");
        a.setAddress1("6133 Some Rd");
        a.setCity("Kansas City");
        a.setState("Missouri");
        //c.setPostalCode("64110");
        a.setCountry("USA");
        assertThat(a.isValid()).isFalse();

        a = new Account();
        a.setName("Test Account");
        a.setAddress1("6133 Some Rd");
        a.setCity("Kansas City");
        a.setState("Missouri");
        a.setPostalCode("64110");
        //c.setCountry("USA");
        assertThat(a.isValid()).isFalse();

        a = new Account();
        a.setName("Test Account");
        a.setAddress1("6133 Some Rd");
        a.setCity("Kansas City");
        a.setState("Missouri");
        a.setPostalCode("64110");
        a.setCountry("USA");
        assertThat(a.isValid()).isTrue();
    }

}
