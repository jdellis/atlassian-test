package com.atlassian.vendors.repositories;

import com.atlassian.vendors.objects.Contact;
import org.springframework.data.repository.CrudRepository;

import java.util.Set;

/**
 * Contact repository
 * @author Josh Ellis - neuroclast@gmail.com
 */
public interface ContactRepository extends CrudRepository<Contact, Integer> {

    // find contact by email, ignoring case
    Contact findContactByEmailIgnoreCase(String email);

    // find all contacts in DB
    Set<Contact> findAll();

}
