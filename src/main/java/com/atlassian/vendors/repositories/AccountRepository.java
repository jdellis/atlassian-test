package com.atlassian.vendors.repositories;

import com.atlassian.vendors.objects.Account;
import org.springframework.data.repository.CrudRepository;

import java.util.Set;

/**
 * Account repository
 * @author Josh Ellis - neuroclast@gmail.com
 */
public interface AccountRepository extends CrudRepository<Account, Integer> {

    // find account by name, ignoring case
    Account findAccountByNameIgnoreCase(String name);

    // find all accounts in DB
    Set<Account> findAll();

}
