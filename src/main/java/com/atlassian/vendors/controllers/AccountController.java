package com.atlassian.vendors.controllers;

import com.atlassian.vendors.objects.Account;
import com.atlassian.vendors.objects.ErrorJson;
import com.atlassian.vendors.repositories.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.Set;

/**
 * Account REST API Controller
 * @author Josh Ellis - neuroclast@gmail.com
 */
@RestController
@RequestMapping("/api/v1/account")
public class AccountController {

    private final AccountRepository accountRepository;


    /**
     * Constructor with dependencies injected
     * @param accountRepository AccountRepository
     */
    @Autowired
    public AccountController(AccountRepository accountRepository) {
        Assert.notNull(accountRepository, "accountRepository cannot be null.");
        this.accountRepository = accountRepository;
    }


    /**
     * Add account to DB
     * @return ResponseEntity
     */
    @PostMapping("")
    public ResponseEntity addAccount(@RequestBody Account account) {
        // check for required fields
        if(!account.isValid()) {
            return ResponseEntity.badRequest().body(new ErrorJson("Required Account fields are not present."));
        }

        // check if Account already exists by matching name
        Account findResult = accountRepository.findAccountByNameIgnoreCase(account.getName());
        if(findResult != null) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new ErrorJson("Account already exists with that name."));
        }

        // save to DB
        accountRepository.save(account);

        // build created URI
        URI createdLocation = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(account.getId())
                .toUri();

        return ResponseEntity.created(createdLocation).body(account);
    }


    /**
     * Retrieve Account from DB
     * @param id Account ID
     * @return ResponseEntity
     */
    @GetMapping("/{id}")
    public ResponseEntity getAccount(@PathVariable("id") Integer id, @RequestParam(required = false, defaultValue = "false") boolean withContacts) {

        // retrieve account from DB
        return accountRepository.findById(id)
                .map(mapAccount -> {

                    // remove contacts if not requested
                    if(!withContacts) {
                        mapAccount.setContacts(null);
                    }

                    return ResponseEntity.ok(mapAccount);
                })
                .orElse(ResponseEntity.notFound().build());
    }


    /**
     * Update existing account
     * @param account Account object
     * @return ResponseEntity
     */
    @PutMapping("")
    public ResponseEntity updateAccount(@RequestBody Account account) {
        // check for required fields
        if(!account.isValid() || account.getId() == null) {
            return ResponseEntity.badRequest().body(new ErrorJson("Required Account fields are not present."));
        }

        // update account
        return accountRepository.findById(account.getId())
                .map(foundAccount -> {
                    // make sure updated name doesn't conflict with existing Account
                    Account resultAccount = accountRepository.findAccountByNameIgnoreCase(account.getName());
                    if(resultAccount != null && !resultAccount.getId().equals(account.getId())) {
                        return ResponseEntity.status(HttpStatus.CONFLICT).body(new ErrorJson("Account already exists with that name."));
                    }

                    // save updated account
                    accountRepository.save(account);

                    return ResponseEntity.ok(account);
                })
                .orElse(ResponseEntity.notFound().build());
    }


    /**
     * List all accounts and their associated contacts
     * @return ResponseEntity
     */
    @GetMapping("")
    public ResponseEntity listAccounts(@RequestParam(required = false, defaultValue = "false") boolean withContacts) {
        // retrieve all accounts from DB
        Set<Account> accounts = accountRepository.findAll();

        // remove contacts unless requested by user
        if(!withContacts) {
            accounts.forEach(account -> account.setContacts(null));
        }

        return ResponseEntity.ok(accounts);
    }
}
