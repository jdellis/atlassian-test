package com.atlassian.vendors.controllers;

import com.atlassian.vendors.objects.Account;
import com.atlassian.vendors.objects.Contact;
import com.atlassian.vendors.objects.ErrorJson;
import com.atlassian.vendors.repositories.AccountRepository;
import com.atlassian.vendors.repositories.ContactRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;

/**
 * Contact REST API Controller
 * @author Josh Ellis - neuroclast@gmail.com
 */
@RestController
@RequestMapping("/api/v1/contact")
public class ContactController {

    private final AccountRepository accountRepository;
    private final ContactRepository contactRepository;


    /**
     * Constructor with dependencies injected
     * @param accountRepository AccountRepository
     * @param contactRepository ContactRepository
     */
    @Autowired
    public ContactController(AccountRepository accountRepository, ContactRepository contactRepository) {
        Assert.notNull(accountRepository, "accountRepository cannot be null.");
        Assert.notNull(accountRepository, "contactRepository cannot be null.");
        this.accountRepository = accountRepository;
        this.contactRepository = contactRepository;
    }


    /**
     * Add Contact to DB
     * @return ResponseEntity
     */
    @PostMapping("")
    public ResponseEntity addContact(@RequestBody Contact contact) {
        // check for required fields
        if(!contact.isValid()) {
            return ResponseEntity.badRequest().body(new ErrorJson("Required Contact fields are not present."));
        }

        // check if contact already exists by matching email
        Contact findResult = contactRepository.findContactByEmailIgnoreCase(contact.getEmail());
        if(findResult != null) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new ErrorJson("Contact already exists with that email address."));
        }

        // check if Contact is associated with Account
        if(contact.getAccount() != null && contact.getAccount().getId() != null) {
            // assign associated Account
            accountRepository.findById(contact.getAccount().getId())
                    .ifPresent(contact::setAccount);
        }

        // save to DB
        contactRepository.save(contact);

        // build created URI
        URI createdLocation = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(contact.getId())
                .toUri();

        return ResponseEntity.created(createdLocation).body(contact);
    }


    /**
     * Retrieve contact from DB
     * @param id Contact ID
     * @return ResponseEntity
     */
    @GetMapping("/{id}")
    public ResponseEntity getContact(@PathVariable("id") Integer id) {
        // retrieve contact from DB
        return contactRepository.findById(id)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }


    /**
     * Update existing contact
     * @param contact Contact object
     * @return ResponseEntity
     */
    @PutMapping("")
    public ResponseEntity updateContact(@RequestBody Contact contact) {
        // check for required fields
        if(!contact.isValid() || contact.getId() == null) {
            return ResponseEntity.badRequest().body(new ErrorJson("Required Contact fields are not present."));
        }

        // update and return contact
        return contactRepository.findById(contact.getId())
                .map(foundContact -> {
                    // make sure updated email doesn't conflict with existing Contact
                    Contact resultContact = contactRepository.findContactByEmailIgnoreCase(contact.getEmail());
                    if(resultContact != null && !resultContact.getId().equals(contact.getId())) {
                        return ResponseEntity.status(HttpStatus.CONFLICT).body(new ErrorJson("Contact already exists with that email."));
                    }

                    contactRepository.save(contact);

                    return ResponseEntity.ok(contact);
                })
                .orElse(ResponseEntity.notFound().build());
    }


    /**
     * Lists all contacts in DB
     * @return ResponseEntity
     */
    @GetMapping("")
    public ResponseEntity listContacts() {
        // find all contacts in DB
        Set<Contact> contactSet = contactRepository.findAll();

        return ResponseEntity.ok(contactSet);
    }
}