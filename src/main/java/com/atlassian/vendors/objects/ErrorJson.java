package com.atlassian.vendors.objects;

/**
 * Error JSON Response Object (immutable)
 * @author Josh Ellis - neuroclast@gmail.com
 */
public final class ErrorJson {
    private final String error;

    public ErrorJson(String status) {
        this.error = status;
    }

    public String getError() {
        return error;
    }
}
