package com.atlassian.vendors.objects;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

/**
 * Contact object
 * @author Josh Ellis - neuroclast@gmail.com
 */
@Entity
public class Contact {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String name;

    @Column(unique = true)
    private String email;

    private String address1;
    private String address2;
    private String city;
    private String state;
    private String postalCode;
    private String country;

    /**
     * Binding if Contact is associated with Account
     */
    @JsonIgnoreProperties({"name", "address1", "address2", "city", "state", "postalCode", "country", "contacts"})
    @ManyToOne
    @JoinColumn(name = "account_id")
    private Account account;


    /**
     * Checks if required value are present
     * @return boolean
     */
    @JsonIgnore
    public boolean isValid() {
        return  name        != null &&
                email       != null &&
                address1    != null &&
                city        != null &&
                state       != null &&
                postalCode  != null &&
                country     != null;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }
}
