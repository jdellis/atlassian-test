package com.atlassian.vendors;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Application entry point
 */
@SpringBootApplication
public class VendorsApplication {

    public static void main(String[] args) {
        SpringApplication.run(VendorsApplication.class, args);
    }
}
