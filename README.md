#Atlassian code exercise - Josh Ellis

## Info

This application was written in Spring Boot, so it can be run quite simply. It should require no external dependencies other than the JRE. The H2 database is used for ease of deployment, and will be created at ~/vendor-db.mv.db on first run.

---

### Starting application

**From IntelliJ / Eclipse**

1. Run as Spring Boot configuration, main class: com.atlassian.vendors.VendorsApplication
2. HTTP server will be started and running on [http://localhost:8080](http://localhost:8080)

**From Maven**

1. `mvnw package`
2. Unit tests will be run, and jar will be output to *target/vendors-0.0.1-SNAPSHOT.jar*
3. `java -jar target/vendors-0.0.1-SNAPSHOT.jar`
4. HTTP server will be started and running on [http://localhost:8080](http://localhost:8080)

That's it!

---

## API Endpoints

There are 8 endpoints exposed to the client, all of which are located under /api/v1:

**Contacts**

| Endpoint                   | Request Type         | Request Body  | Parameters                |
| -------------------------- | -------------------- | ------------- | ------------------------- |
| /api/v1/contact            | POST                 | Contact       | none                      |
| /api/v1/contact            | PUT                  | Contact       | none                      |
| /api/v1/contact            | GET                  | none          | none                      |
| /api/v1/contact/{id}       | GET                  | none          | none                      |

**Accounts**

| Endpoint                   | Request Type         | Request Body  | Parameters                |
| -------------------------- | -------------------- | ------------- | ------------------------- |
| /api/v1/account            | POST                 | Account       | none                      |
| /api/v1/account            | PUT                  | Account       | none                      |
| /api/v1/account            | GET                  | none          | withContacts=(true/false) |
| /api/v1/account/{id}       | GET                  | none          | withContacts=(true/false) |

---

## Objects

**Account**
```
{
    "id": Integer,
    "name": String,
    "address1": String,
    "address2": String,
    "city": String,
    "state": String,
    "postalCode": String,
    "country": String,
    "contacts": []  // optional based on withContacts boolean
}
```

**Contact**
```
{
    "id": Integer,
    "name": "Josh Ellis",
    "email": String,
    "address1": String,
    "address2": String,
    "city": String,
    "state": String,
    "postalCode": String,
    "country": String,
    "account": {    // only shown if Contact is associated with Account
        "id": Integer
    }
}
```

**JsonResponse**
```
{
    "status": "success" or "failure",
    "payload": {} or []
}
```

---

## Postman Testing

A Postman collections file has been supplied in the repository *(Atlassian Tests.postman_collection.json)* which can be imported into Postman. This can be used to test the various endpoints and as a guide to how to use them.